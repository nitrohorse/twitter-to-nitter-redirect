# Twitter to Nitter Redirect

[![Downloads](https://img.shields.io/amo/users/twitter-to-nitter-redirect.svg)](https://addons.mozilla.org/en-US/firefox/addon/twitter-to-nitter-redirect)
[![License](https://img.shields.io/badge/licensed-ethically-%234baaaa)](https://firstdonoharm.dev/)

Browser extension that redirects mobile.twitter.com and twitter.com link to [nitter.net](https://github.com/zedeus/nitter) links!

## Download
* [Firefox](https://addons.mozilla.org/en-US/firefox/addon/twitter-to-nitter-redirect/)

## Install Locally
* Firefox: [temporary](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox)
* Chrome: [permanent](https://superuser.com/questions/247651/how-does-one-install-an-extension-for-chrome-browser-from-the-local-file-system/247654#247654)

## Local Development Setup
* Clone the repo
* Install tools:
	* [Node.js](https://nodejs.org)
	* [nvm](https://github.com/nvm-sh/nvm)
* Use specified Node version:
	* `nvm use`
* Install dependencies:
	* `npm i`
* Package for distribution:
	* `npm run bundle`

## Develop Locally in Firefox
* Run the extension in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) (open the [Browser Toolbox](https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox) for console logging):
	* `npm start`

## Develop Locally in Chromium
* Open Chromium and navigate to `chrome://extensions/`.
* Enabled "Developer mode" and select "Load unpacked."
* Open the `web-extension` directory to run the extension.
* Use the [Chrome Apps & Extensions Developer Tool](https://chrome.google.com/webstore/detail/chrome-apps-extensions-de/ohmmkhmmmpcnpikjeljgnaoabkaalbgc?utm_source=chrome-app-launcher-info-dialog) for debugging.

## Resources
* Icon by [Icons8](https://icons8.com/)
