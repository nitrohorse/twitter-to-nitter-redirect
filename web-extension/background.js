const nitter = 'https://nitter.net';

chrome.webRequest.onBeforeRequest.addListener(
	(details) => {
		const url = new URL(details.url);
		
		if (url.hostname === 'nitter.net') {
			return;
		}
		
		return {
			redirectUrl: `${nitter}${url.pathname}${url.search}${url.hash}`
		};
	},
	{
		urls: [
			"*://mobile.twitter.com/*",
			"*://twitter.com/*"
		],
		types: [
			'main_frame',
			'sub_frame',
			'stylesheet',
			'script',
			'image',
			'object',
			'xmlhttprequest',
			'other'
		]
	},
	['blocking']
);
